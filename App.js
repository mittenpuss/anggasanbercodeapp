import 'react-native-gesture-handler';
import React, { useEffect } from 'react'
import {View,Text,StyleSheet,StatusBar} from 'react-native'
import AuthStack from './src/navigation/AuthStack'
import firebase from '@react-native-firebase/app';


var firebaseConfig = {
  apiKey: "AIzaSyDJ7gIMEU_1TKbED6mEPx_rirtCotxU49k",
  authDomain: "anggasanber.firebaseapp.com",
  databaseURL: "https://anggasanber.firebaseio.com",
  projectId: "anggasanber",
  storageBucket: "anggasanber.appspot.com",
  messagingSenderId: "895187050853",
  appId: "1:895187050853:web:ad1895eb1c9e5005bc818c",
  measurementId: "G-T4SEHE0YDL"
};

// Inisialisasi firebase
if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}


const App=()=>{

  return (
        <AuthStack/>
  )
}


export default App;