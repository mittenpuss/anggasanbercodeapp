import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  
  slide: {
    flex: 1,
  },
  title: {
    color: '#191970',
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  image: {
    alignSelf: 'center',
  },
  text: {
    color: '#a4a4a6',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
    marginHorizontal: 20,
  },
  buttonCircle: {
    borderRadius: 100,
    width: 45,
    height: 45,
    backgroundColor: '#191970',
    alignItems: 'center',
    justifyContent: 'center',
  },
  onBoardingContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
