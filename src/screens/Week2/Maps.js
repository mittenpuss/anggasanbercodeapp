import React, { useEffect } from 'react'
import {View,Text} from 'react-native'
import MapboxGL from '@react-native-mapbox-gl/maps'

MapboxGL.setAccessToken('pk.eyJ1IjoiYW5nZ2Fhd2lqYXlhIiwiYSI6ImNrZXRzOW1uODFodnYycW1yZ3B6NWFqaXIifQ.SadIQIe4ZPUbhLalGMltRw')


const Map = () =>{

    useEffect(()=>{
        const getLocation = async () =>{
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions()
            }
            catch (err) {
                console.log(err)
            }
        }
        getLocation()
    },[])

    const coordinates = [
        [107.598827, -6.896191],
        [107.596198, -6.899688],
        [107.618767, -6.902226],
        [107.621095, -6.898690],
        [107.615698, -6.896741],
        [107.613544, -6.897713],
        [107.613697, -6.893795],
        [107.610714, -6.891356],
        [107.605468, -6.893124],
        [107.609180, -6.898013]
    ]

    return (
        <View style={{height:'100%',width:'100%'}}>
            <MapboxGL.MapView
            style={{flex:1}}>
                <MapboxGL.UserLocation
                    visible={true}
                />

                
                <MapboxGL.Camera
                followUserLocation={true}
                />

                {coordinates.map((item,index)=>{
                    return (
                        <MapboxGL.PointAnnotation
                            key={index}
                            id={`Annotation${index}`}
                            coordinate={[item[0],item[1]]}    
                        >
                        <MapboxGL.Callout
                            title={`Longitude: ${item[0]}
                            \n Latitude: ${item[1]}`}
                        />
                    </MapboxGL.PointAnnotation>
                    )
                })}
            

            </MapboxGL.MapView>
        </View>
    )
}

export default Map