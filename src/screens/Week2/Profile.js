import React, { useEffect,useState } from 'react'
import { View, Text, StyleSheet, Image, Button } from 'react-native';
import FotoProfil from '../../assets/images/defaultPic.png'
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import { GoogleSignin } from '@react-native-community/google-signin';
import { CommonActions } from '@react-navigation/native';

const Profile = ({navigation}) =>{

    const [userInfo,setUserInfo] = useState(null)

    useEffect(()=>{
        
        async function getToken() {
            try {
                const token = await AsyncStorage.getItem('token')
                console.log(token)
                // return getVenue(token)
            }
            catch (err) {
                console.log(err)
            }
        }
        getToken()
        GoogleSignin.configure({});
        getCurrentUser()
    },[])

    const getCurrentUser = async () =>{
        const userInfo = await GoogleSignin.signInSilently()
        setUserInfo(userInfo)
    }

    // const getVenue = (token) => {
    //     Axios.get(`https://mainbersama.demosanbercode.com/api/venues`, {
    //         timeout: 20000,
    //         headers: {
    //             'Authorization': 'Bearer' + token
    //         }
    //     })
    //     .then((res)=>{

    //     })
    //     .catch((err)=>{
    //         console.log(err)
    //     })
    // }

    const onPressLogout= async ()=>{
        try {
            await GoogleSignin.revokeAccess()
            await GoogleSignin.signOut()
            await AsyncStorage.removeItem('token')
            navigation.dispatch(
                CommonActions.reset({
                    index:0,
                    routes: [{ name: 'Login'}]
                })
            ) 
        }
        catch (err) {
            console.log(err)
            AsyncStorage.removeItem('token')
            .then((res)=>{
                navigation.dispatch(
                    CommonActions.reset({
                        index:0,
                        routes: [{ name: 'Login'}]
                    })
                )                
            })
            .catch((err)=>{
                console.log(err)
            })

        }
    }


    
    return (
        <View>
            <Text>
                Ini Profile
            </Text>
            <Button
                title='Logout'
                onPress={()=> onPressLogout()}    
            >
            </Button>

            <Text>
                {userInfo && userInfo.user && userInfo.user.photo}
            </Text>
            <Text>
                {userInfo && userInfo.user && userInfo.user.name}
            </Text>
            <Text>
                {userInfo && userInfo.user && userInfo.user.email}
            </Text>
            

        </View>
    )

}

const styles = StyleSheet.create({
    bg: {
        height: '40%',
        width: '100%',
        backgroundColor:'#3EC6FF',
        alignItems: 'center',
    },
    containerFoto: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:'15%'
    },
    foto: {
        width:80,
        height:80, 
        borderRadius:50
    },
    textFoto: {
        color:'white', 
        textAlign:'center', 
        paddingTop:15,
        fontSize:17
    },
    containerData: {
        backgroundColor:'white', 
        width:'80%', 
        height:260,
        marginTop:30,
        borderRadius:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    textData1: {
        marginTop:15,
        marginLeft:10,
        flex:1,
    },
    textData2: {
        marginTop:15,
        marginLeft:10,
        flex:2,
        textAlign:'right',
        marginRight:10
    },
    button: {
        width: '80%',
        marginTop:20,
        alignSelf:'center'
    },

})

const FotoData=()=>{
    return(
        <View style={styles.containerFoto}>
            <Image 
                source={FotoProfil} 
                style={styles.foto}>
            </Image>
            <Text style={styles.textFoto}>
                Angga Wijaya
            </Text>
        </View>
    )
}

const IsiData=()=>{
    return (
    <View style={styles.containerData}>
        <View style={{flexDirection:'row'}}>
            <Text style={styles.textData1}>
                Tanggal Lahir
            </Text>
            <Text style={styles.textData2}>
                7 September 1994
            </Text>
        </View>
    
        <View style={{flexDirection:'row'}}>
            <Text style={styles.textData1}>
                Jenis Kelamin
            </Text>
            <Text style={styles.textData2}>
                Laki - Laki
            </Text>
        </View>

        <View style={{flexDirection:'row'}}>
            <Text style={styles.textData1}>
                Hobi
            </Text>
            <Text style={styles.textData2}>
                Main Game
            </Text>
        </View>

        <View style={{flexDirection:'row'}}>
            <Text style={styles.textData1}>
                No. Telp
            </Text>
            <Text style={styles.textData2}>
                081236046472
            </Text>
        </View>

        <View style={{flexDirection:'row'}}>
            <Text style={styles.textData1}>
                Email
            </Text>
            <Text style={styles.textData2}>
                anggaa_wijaya@Yahoo.com
            </Text>
        </View>

        <View style={styles.button}>
            <Button
                title='Logout'
                color='#41C4FF'
                onPress={onPressLogout}
            >
            </Button>
        </View>

    </View>
    )
}


export default Profile;