import React, { useState } from 'react'
import { View, Text, processColor } from 'react-native'
import {BarChart} from 'react-native-charts-wrapper'


const Chart = () =>{

    //Jangan disentuh
    const data = [
        {y:[100, 40], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[80, 60], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[40, 90], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[78, 45], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[67, 87], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[98, 32], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[150, 90], marker: ["React Native Dasar", "React Native Lanjutan"]},
    ]


    //Looping Ganti isi data
    // for(var i=0;i<data.length;i++){
    //     for(var j=0;j<data[i].marker.length;j++){
    //         if(j%2===0){
    //             data[i].marker[j] = `React Native Dasar \n ${data[i].y[j]}`
    //         }else{
    //             data[i].marker[j] = `React Native Lanjutan \n ${data[i].y[j]}`
    //         }
    //     }
    // }


    const newData = () => {
        const data = [
            [100, 40],
            [80, 60],
            [40, 90],
            [78, 45],
            [67, 87],
            [98, 32],
            [150, 90]
        ]

        return data.map((val,index) => {
            return {
                y: [val[0],val[1]],
                marker: [`React Native Dasar ${val[0]}`, `React Native Lanjutan ${val[1]}`]
            }
        })
    }

    const [legend, setLegend] = useState({
        enabled: true,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })

    const [chart, setChart] = useState({
        data: {
            dataSets: [{
                values: newData(),
                label: '',
                config: {
                    colors: [processColor('#41C4FF'), processColor('darkblue')],
                    stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                    drawFilled: false,
                    drawValues: false,
                }
            }]
        }
    })

    const [xAxis, setXAxis] = useState({
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
        position: 'BOTTOM',
        drawAxisLine: true,
        drawGridLines: false,
        axisMinimum: -0.5,
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: new Date().getMonth(),
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
    })

    const [yAxis, setYAxis] = useState({
        left: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
        right: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            enabled: false
        }
    })
    
    return (
        <View style={{flex:1}}>
            <BarChart
                style={{flex:1}}
                data={chart.data}
                yAxis={yAxis}
                xAxis={xAxis}
                legend={legend}
                doubleTapToZoomEnabled={false}
                chartDescription={{text: ''}}
                marker={{
                    enabled:true,
                    textColor:'white',
                    textSize:14,
                    markerColor:'grey'
                }}

            >

            </BarChart>
        </View>
    )
}

export default Chart



// CONTOH
// const [legend,setLegend] = useState({
//     enabled: false,
//     textSize: 14,
//     form: 'SQUARE',
//     formSize: 14,
//     xEntrySpace: 10,
//     yEntrySpace: 5,
//     formToTextSpace: 5,
//     wordWrapEnabled: true,
//     maxSizePercent: 0.5
// })

// const [chart,setChart] = useState({
//     data: {
//         dataSets: [{
//             values: [{y: 100, marker: 'Contoh Detail Marker'}, {y: 105}, {y: 102}, {y: 110}, {y: 114}, {y: 109}, {y: 105}],
//             label: '',
//             config: {
//                 // colors: [processColor(colors.blue)],
//                 stackLabels: [],
//                 drawFilled: false,
//                 drawValues: false,
//             }
//         }]
//     }
// })

// const [xAxis, setXAxis] = useState({
//     valueFormatter: ['Jan','Feb','Mar','Apr','May',"Jun","Jul",'Agu','Sep','Oct','Nov','Dec'],
//     position: 'BOTTOM',
//     drawAxisLine: true,
//     drawGridLines: false,
//     axisMinimum: -0.5,
//     granularityEnabled: true,
//     granularity: 1,
//     axisMaximum: new Date().getMonth() + 0.5,
//     spaceBeetweenLabels: 0,
//     labelRotationAngle: -45.0,
//     limitLines: [{limit: 115, lineColor: processColor('red'), lineWidth:1 }]
// })

// const [yAxis,setYAxis] = useState({
//     left: {
//         axisMinimum: 0,
//         labelCountForce: true,
//         granularity: 5,
//         granularityEnabled: true,
//         drawGridLines: false
//     },

//     right: {
//         axisMinimum: 0,
//         labelCountForce: true,
//         granularity: 5,
//         granularityEnabled: true,
//         enabled: false
//     }
// })