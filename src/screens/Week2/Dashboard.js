import React, { useEffect } from 'react'
import {View,Text, ScrollView, StyleSheet, StatusBar, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';
import AutoHeightImage from 'react-native-auto-height-image';
import AsyncStorage from '@react-native-community/async-storage';

const DashBoard = ({navigation}) =>{

    useEffect(()=>{
        async function getToken() {
            try {
                const token = await AsyncStorage.getItem('token')
                console.log(token)
            }
            catch (err) {
                console.log(err)
            }
        }
        getToken()
    },[])


    return (
       
        <ScrollView style={{backgroundColor:'white'}}>
            <StatusBar barStyle='dark-content' backgroundColor="#ffffff" />
            
            {/* KELAS TOP */}
            <View style={styles.containerMain}>
                <View style={styles.containerTop}>
                    <Text style={styles.containerTopHeader}>
                        Kelas
                    </Text>
                </View>
                <View style={styles.containerBottom}>
                    <View style={styles.containerBottomData}>
                        <TouchableOpacity onPress={()=> navigation.navigate('React Native')} style={styles.containerBottomData}>
                        <Icon
                            name='logo-react'
                            color='white'
                            size={50}>

                        </Icon>
                        <Text style={styles.containerBottomText}>
                            React Native
                        </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerBottomData}>
                        
                        <Icon
                            name='logo-python'
                            color='white'
                            size={50}>

                        </Icon>
                        <Text style={styles.containerBottomText}>
                            Data Science
                        </Text>
                        
                        
                    </View>
                    <View style={styles.containerBottomData}>
                        <Icon
                            name='logo-react'
                            color='white'
                            size={50}>

                        </Icon>
                        <Text style={styles.containerBottomText}>
                            React JS
                        </Text>
                    </View>
                    <View style={styles.containerBottomData}>
                        <Icon
                            name='logo-laravel'
                            color='white'
                            size={50}>

                        </Icon>
                        <Text style={styles.containerBottomText}>
                            Laravel
                        </Text>
                    </View>
                </View>
            </View>

            {/* KELAS BOTTOM */}
            <View style={styles.containerMain}>
                <View style={styles.containerTop}>
                    <Text style={styles.containerTopHeader}>
                        Kelas
                    </Text>
                </View>
                <View style={styles.containerBottom}>
                    <View style={styles.containerBottomData}>
                        <Icon
                            name='logo-wordpress'
                            color='white'
                            size={50}>

                        </Icon>
                        <Text style={styles.containerBottomText}>
                            Wordpress
                        </Text>
                    </View>
                    <View style={styles.containerBottomData}>
                        <AutoHeightImage
                            width={50}
                            source={require('../../assets/icons/website-design.png')}
                        />
                        <Text style={styles.containerBottomText}>
                            Design Grafis
                        </Text>
                    </View>
                    <View style={styles.containerBottomData}>
                        <Icon
                            name='server'
                            color='white'
                            size={50}>

                        </Icon>
                        <Text style={styles.containerBottomText}>
                            Web Server
                        </Text>
                    </View>
                    <View style={styles.containerBottomData}>
                        <AutoHeightImage
                            width={50}
                            source={require('../../assets/icons/ux.png')}
                        />
                        <Text style={styles.containerBottomText}>
                            UI/UX Design
                        </Text>
                    </View>
                </View>
                
            </View>

            {/* SUMMARY */}
            <View style={styles.containerMain}>
                <View style={styles.containerTop}>
                    <Text style={styles.containerTopHeader}>
                        Summary
                    </Text>
                </View>

                {/* REACT NATIVE */}
                <View style={styles.containerIsiBiruMuda}>
                    <Text style={styles.containerIsiBiruMudaText}>
                        React Native
                    </Text>
                </View>
                {/* ISI REACT NATIVE */}
                <View style={styles.containerIsiBiruTua}>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Today
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            20 Orang
                        </Text>
                    </View>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Total
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            100 Orang
                        </Text>
                    </View>
                </View>

                {/* DATA SCIENCE */}
                <View style={styles.containerIsiBiruMuda}>
                    <Text style={styles.containerIsiBiruMudaText}>
                        Data Science
                    </Text>
                </View>
                {/* ISI DATA SCIENCE */}
                <View style={styles.containerIsiBiruTua}>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Today
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            30 Orang
                        </Text>
                    </View>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Total
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            100 Orang
                        </Text>
                    </View>
                </View>

                {/* REACT JS */}
                <View style={styles.containerIsiBiruMuda}>
                    <Text style={styles.containerIsiBiruMudaText}>
                        ReactJS
                    </Text>
                </View>
                {/* ISI REACT JS */}
                <View style={styles.containerIsiBiruTua}>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Today
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            66 Orang
                        </Text>
                    </View>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Total
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            100 Orang
                        </Text>
                    </View>
                </View>

                {/* LARAVEL */}
                <View style={styles.containerIsiBiruMuda}>
                    <Text style={styles.containerIsiBiruMudaText}>
                        Laravel
                    </Text>
                </View>
                {/* ISI LARAVEL */}
                <View style={styles.containerIsiBiruTua}>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Today
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            60 Orang
                        </Text>
                    </View>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Total
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            100 Orang
                        </Text>
                    </View>
                </View>

                {/* WORDPRESS */}
                <View style={styles.containerIsiBiruMuda}>
                    <Text style={styles.containerIsiBiruMudaText}>
                        Wordpress
                    </Text>
                </View>
                {/* ISI LARAVEL */}
                <View style={styles.containerIsiBiruTua}>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Today
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            30 Orang
                        </Text>
                    </View>
                    <View style={styles.containerIsiBiruTuaHeight}>
                        <Text style={styles.containerIsiBiruTuaText}>
                            Total
                        </Text>
                        <Text style={styles.containerIsiBiruTuaText}>
                            100 Orang
                        </Text>
                    </View>
                </View>



            </View>


        </ScrollView>
    )
} 

const styles = StyleSheet.create({
    containerMain: {
        margin:10,
        backgroundColor:'white'
    },
    containerTop: {
        height:40,
        backgroundColor:'#088dc4',
        justifyContent:'center',
        borderTopLeftRadius:10,
        borderTopRightRadius:10
    },
    containerTopHeader: {
        color:'white',
        marginLeft:15
    },
    containerBottom:{
        flexDirection:'row',
        height:90,
        justifyContent:'center',
        backgroundColor:'#3EC6FF',
        borderBottomLeftRadius:10,
        borderBottomRightRadius:10
    },
    containerBottomData: {
        flex:1,
        justifyContent:'center', 
        alignItems:'center',
        margin:5
    },
    containerBottomText: {
        color:'#ffffff',
        fontSize:11
    },
    containerIsiBiruMuda: {
        height:35,
        color:'white',
        justifyContent:'center',
        backgroundColor:'#3EC6FF',
    },
    containerIsiBiruMudaText: {
        color:'white',
        marginLeft:15,
        fontWeight:'bold',
        fontSize:14
    },
    containerIsiBiruTua: {
        flexDirection:"column",
        backgroundColor:'#088dc4'
    },
    containerIsiBiruTuaHeight: {
        flexDirection:'row',
        justifyContent:'space-around', 
        height:30, 
        alignItems:'center',
    },
    containerIsiBiruTuaHeight: {
        flexDirection:'row',
        justifyContent:'space-around', 
        height:30, 
        alignItems:'center'
    },
    containerIsiBiruTuaText: {
        color:'white'
    }

}) 

export default DashBoard