import React, { useState, useEffect } from 'react';
import { View, Image,StyleSheet,TextInput,Button,Text,StatusBar} from 'react-native';
import Logo from '../../assets/images/logo.jpg'
import AutoHeightImage from 'react-native-auto-height-image';
import {Input} from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios'
import auth from '@react-native-firebase/auth'
import {GoogleSignin, statusCodes, GoogleSigninButton} from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id';
import { CommonActions } from '@react-navigation/native';



const Login = ({ navigation }) => {

    const [email,onChangeEmail] = useState('')
    const [password,onChangePassword] = useState('')

    useEffect(()=>{
        configureGoogleSignIn()
    },[])

    const config = {
        title: 'Authentication Required',
        imageColor : '#191970',
        imageErrorColor :'red',
        sensorDescription: 'Touch Sensor',
        sensorErrorDescription: 'Failed',
        cancelText: 'Cancel'
    }


    const saveToken = async (token) => {
        try {
            await AsyncStorage.setItem('token', token)
        }
        catch(err) {
            console.log(err)
        }
    }

    


    const signInWithFingerPrint=()=>{
        TouchID.authenticate('',config)
        .then(success=>{
            alert('Authentication Success')
            navigation.dispatch(
                CommonActions.reset({
                    index:0,
                    routes: [{ name: 'HomeTab'}]
                })
            ) 
        }).catch(err=>{
            alert('Authentication Failed')
        })
      }

    const configureGoogleSignIn=()=>{
        GoogleSignin.configure({
            offlineAccess:false,
            webClientId:'895187050853-i06v6l4vhkd6akom4i8c9hlj6sijgeg5.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () =>{
        try {
            const { idToken } = await GoogleSignin.signIn()

            const credential = auth.GoogleAuthProvider.credential(idToken)
            auth().signInWithCredential(credential)
            
            saveToken(idToken)

            navigation.dispatch(
                CommonActions.reset({
                    index:0,
                    routes: [{ name: 'HomeTab'}]
                })
            ) 
        }
        catch (err) {
            console.log(err)
        }
    }
   

    const onPressLogin = () =>{

        if(email==='' || password===''){
            alert('input masih kosong')
        }else{

            return auth().signInWithEmailAndPassword(email,password)
            .then((res)=>{
                navigation.dispatch(
                    CommonActions.reset({
                        index:0,
                        routes: [{ name: 'HomeTab'}]
                    })
                )
            })
            .catch((err)=>{
                alert('username atau password salah')
            }) 

        }


    }

    // const onPressLogin=()=>{

    //     if(email === '' || password === ''){
    //         alert('Email atau Password masih kosong, mohon dilengkapi.')
    //     }else{

    //         let data = {
    //             email:email,
    //             password:password
    //         }

    //         Axios.post(`https://mainbersama.demosanbercode.com/api/login`, data, {
    //         timeout: 20000
    //         })
    //         .then((res)=>{
    //             console.log(res.data)
    //             saveToken(res.data.token)
    //             onChangePassword('')
    //             onChangeEmail('')
    //             navigation.navigate('HomeTab')
    //         })
    //         .catch((err)=>{
    //             console.log(err)
    //             onChangePassword('')
    //             onChangeEmail('')
    //         })

    //     }

    // }
      
    return (
        <View style={styles.container}>

        <StatusBar barStyle='dark-content' backgroundColor="white" />

            <View style={styles.imageContainer}>
                <AutoHeightImage
                    width={300}
                    source={Logo}
                />
            </View>

            <View style={{width:'80%'}}>
                <Input
                    leftIcon={
                        <Icon
                            name='user'
                            size={24}
                            color='black'
                        />
                    }
                    placeholder='Username or Email'
                    label='Email'
                    onChangeText={username => onChangeEmail(username)}
                    value={email}
                />
            </View>

            <View style={{width:'80%'}}>
                <Input
                    leftIcon={
                        <Icon
                            name='lock'
                            size={24}
                            color='black'
                        />
                    }
                    placeholder='Password'
                    label='Password'
                    secureTextEntry={true}
                    onChangeText={password => onChangePassword(password)}
                    value={password}

                />
            </View>

            <View style={styles.button}>
            
            <Button
            title='Login'
            color='#41C4FF'
            onPress={onPressLogin}
            >
            </Button>

            <View style={{flexDirection: 'row', alignItems: 'center', paddingTop:10}}>
                <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
                    <View>
                        <Text style={{width: 50, textAlign: 'center'}}>OR</Text>
                    </View>
                <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
                </View>
            </View>

            <GoogleSigninButton
                onPress={() => signInWithGoogle()}
                style={{width:'80%', height:50}}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Dark}>
            </GoogleSigninButton>

            <View style={{paddingTop:10,width: '80%',alignSelf:'center'}}>
                    <Button
                        title='SIGN IN WITH FINGERPRINT'
                        color='#191970'
                        onPress={signInWithFingerPrint}
                    >
                    
                    </Button> 
            </View>

            <Text style={styles.createAccount}>
                Belum Mempunyai akun? Buat{` `}
                <Text style={{fontWeight:'bold'}} onPress={()=> navigation.navigate('Register')}>Akun</Text>
            </Text>
       

        
        
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,      
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
    },
    imageContainer:{
        marginBottom:'10%',
        justifyContent: 'center', 
        alignItems:'center'
    },
    logo: {
        width: '100%', 
        height: 100,
       
      },
    textInputEmail:{
        marginTop:10, 
        width: '80%', 
        borderColor: "red", 
        borderWidth: 0.5, 
        borderRadius:40,
        alignSelf:'center',
        textAlign:'center'
    },
    textInputPassword:{
        marginTop:20,
        width: '80%', 
        borderColor: "#41C4FF", 
        borderWidth: 0.5, 
        alignSelf:'center',
        textAlign:'center'
    },
    button: {
        width: '80%',
        marginTop:20,
        alignSelf:'center'
    },
    createAccount: {
        marginTop:50,
        alignSelf:'center',
    }
      
})

export default Login;