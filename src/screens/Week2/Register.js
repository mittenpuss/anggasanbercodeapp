import React, { useState } from 'react'
import {View,Text,StyleSheet,Image,TouchableOpacity,TextInput,ScrollView, KeyboardAvoidingView, Button, Modal} from 'react-native'
import {RNCamera} from 'react-native-camera'
import storage from '@react-native-firebase/storage'
import FotoProfil from '../../assets/images/defaultPic.png'
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const Register = ({ navigation }) => {


    const [isVisible,setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo,setPhoto] = useState(null)

    const toggleCamera = () =>{
        setType(type === 'back' ? 'front' : 'back')
    }

    const takePicture = async () => {
        const options = {quality: 0.5, base64: true}
        if(camera) {
            const data = await camera.takePictureAsync(options)
            setPhoto(data)
            setIsVisible(false)
        }
    }
    
    const uploadImage=(uri)=>{
        const sessionId = new Date().getTime()
        return storage()
        .ref(`images/${sessionId}`)
        .putFile(uri)
        .then((res)=>{
            alert('Upload Success')
        })
        .catch((error)=>{
            alert(error)
        })
    }

    return (

        <View style={{flex:1}}>
            
            {/* Modal RN Camera */}
            <Modal visible={isVisible} onRequestClose={()=> setIsVisible(false)}>
               <View style={{flex:1}}>
                    <RNCamera
                        style={{flex:1}}
                        ref={ref => {
                            camera = ref
                        }}
                        captureAudio={false}
                        type={type}
                    >
                    
                        <View style={styles.btnFlipContainer}>
                            <TouchableOpacity style={styles.btnFlip} onPress={toggleCamera}>
                                <MaterialCommunity name='rotate-3d-variant' size={25}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.round}/>
                        <View style={styles.rectangle}/>
                        <View style={styles.btnTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={takePicture}>
                                <Icon name='camera' size={30} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
               </View>
            </Modal>

            <View style={styles.bg}>
                <View style={styles.containerFoto}>
                    <Image 
                        source={photo === null ? FotoProfil : { uri: photo.uri } }
                        style={styles.foto}>
                    </Image>
                    <TouchableOpacity onPress={()=>setIsVisible(true)}>
                        <Text style={styles.textFoto}>
                            Change Picture
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.containerData}>
                <View style={styles.data}>
                    <Text style={{fontWeight:'bold'}}>
                        Nama
                    </Text>
                    <TextInput
                    placeholder='Input nama'
                    style={{borderBottomWidth:0.5,width:'100%'}}>
                    </TextInput>

                    <Text style={{marginTop:20,fontWeight:'bold'}}>
                        Email
                    </Text>
                    <TextInput
                    placeholder='Input email'
                    style={{borderBottomWidth:0.5,width:'100%'}}>
                    </TextInput>

                    <Text style={{marginTop:20,fontWeight:'bold'}}>
                        Password
                    </Text>
                    <TextInput
                    placeholder='Input password'
                    style={{borderBottomWidth:0.5,width:'100%',marginBottom:20}}>
                    </TextInput>

                    <Button
                        title='REGISTER'
                        color='#3EC6FF'
                        onPress={()=> uploadImage(photo.uri)}
                    >
                    </Button> 

                </View>
            </View>
        
        </View>      
       
     

        
    )
}

const styles = StyleSheet.create({
    bg: {
        height: '40%',
        width: '100%',
        backgroundColor:'#3EC6FF',
        alignItems: 'center',
    },
    containerFoto: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:'20%'
    },
    foto: {
        width:80,
        height:80, 
        borderRadius:50
    },
    textFoto: {
        color:'white', 
        textAlign:'center', 
        paddingTop:15,
        fontSize:17
    },
    containerData: {
        marginLeft:30,
        marginRight:30,
        marginTop:-80,
        borderRadius:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
        backgroundColor:'white',
    },
    data: {
        margin:15
    },
    btnFlipContainer: {
        height:50,
        marginLeft:10,
        marginTop:20

    },
    btnFlip: {
        width:40,
        height:40,
        borderRadius:20,
        backgroundColor:'white',
        alignItems:'center',
        justifyContent: 'center',
        
    },
    round: {
        justifyContent:'center',
        alignItems:'center',
        borderWidth:1,
        borderRadius:100,
        borderColor:'white',
        height:'35%',
        marginLeft:'25%',
        marginRight:'25%',
        marginTop:20
        
    },
    rectangle: {
        justifyContent:'center',
        alignItems:'center',
        borderWidth:1,
        borderColor:'white',
        height:'15%',
        marginLeft:'25%',
        marginRight:'25%',
        marginTop:'20%'
    },
    btnTakeContainer: {
        height:50,
        marginTop:20,
        justifyContent:'center',
        alignItems:'center'
    },
    btnTake: {
        marginTop:160,
        width:60,
        height:60,
        borderRadius:60,
        backgroundColor:'white',
        alignItems:'center',
        justifyContent: 'center',
    }

})

export default Register


// const renderCamera=()=>{
//     return (
//         <Modal visible={isVisible} onRequestClose={()=> setIsVisible(false)}>
//            <View style={{flex:1}}>
//                 <RNCamera
//                     style={{flex:1}}
//                     ref={ref => {
//                         camera = ref
//                     }}
//                     captureAudio={false}
//                     type={type}
//                 >
                
//                     <View style={styles.btnFlipContainer}>
//                         <TouchableOpacity style={styles.btnFlip} onPress={toggleCamera}>
//                             <MaterialCommunity name='rotate-3d-variant' size={15}/>
//                         </TouchableOpacity>
//                     </View>
//                     <View style={styles.round}/>
//                     <View style={styles.rectangle}/>
//                     <View style={styles.btnTakeContainer}>
//                         <TouchableOpacity style={styles.btnTake}>
//                             <Icon name='camera' size={30} />
//                         </TouchableOpacity>
//                     </View>
//                 </RNCamera>
//            </View>
//         </Modal>
//     )
// }
