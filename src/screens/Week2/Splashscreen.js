import React from 'react';
import { View, Image, StyleSheet } from "react-native";
import AutoHeightImage from 'react-native-auto-height-image';

function SplashScreen() {
    return (
        <View style={styles.container}>

        <View style={styles.logoContainer}>
            <AutoHeightImage
                width={300}
                source={require('../../assets/images/logo.jpg')}
            />
        </View>    
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height:'100%',
        width:'100%',
        backgroundColor:'white'
    },
    logoContainer: {
        height:'100%',
        alignItems: 'center',
        justifyContent: "center",

    }
})

export default SplashScreen;