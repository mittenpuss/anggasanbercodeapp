import React, { useEffect, useState } from 'react'
import { View, Text } from 'react-native'

import database from '@react-native-firebase/database';
import { GiftedChat } from 'react-native-gifted-chat'

import auth from '@react-native-firebase/auth'

const Chat = ({route, navigation}) => {

    const [messages, setMessages] = useState([])
    const [user,setUser] = useState({})

    useEffect(()=>{
        const user = auth().currentUser
        setUser(user)
        getData()
        return () => {
            const db = database().ref('messages')
            if(db) {
                db.off()
            }
        }
    },[])
    

    //gambar bisa dilihat di folder week2

    const getData = () =>{
        database().ref('messages').limitToLast(20).on('child_added', snapshot => {
            const value = snapshot.val()
            setMessages(previousMessages => GiftedChat.append(previousMessages,value))
        })
    }

    const onSend = (( messages = [] ) =>{
        for(let i=0; i<messages.length; i++){
            database().ref('messages').push({
                _id: messages[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            }) 
        }
    } )

    return (
        <GiftedChat
            messages={messages}
            onSend={messages => onSend(messages)}
            user={{
                _id: user.uid,
                name: user.email,
                avatar: 'https://cdn.iconscout.com/icon/free/png-512/avatar-370-456322.png'
            }}

        >

        </GiftedChat>
    )
}

export default Chat