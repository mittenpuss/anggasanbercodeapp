import React, { useContext } from 'react'
import {View, Text, TextInput, Button, TouchableOpacity, FlatList} from 'react-native'
import {RootContext} from './Jawaban2'

const Consumer = () =>{
    
    const state = useContext(RootContext)
 
    console.log(state)

    return (
        <View>
            <Text>
                Mantap ini consumer
            </Text>
        </View>
        )

}

export default Consumer