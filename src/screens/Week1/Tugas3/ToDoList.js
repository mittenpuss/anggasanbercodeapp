import React, { useState } from 'react'
import {View, Text, TextInput, Button, TouchableOpacity, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const ToDoList = () =>{
    
    const [data,setData]=useState([
        {id:'1',name:'Membuat Login Screen',tanggalInput:'25/8/2020'},
        {id:'2',name:'Membuat Logout Screen',tanggalInput:'25/8/2020'},
        {id:'3',name:'Membuat Menu Screen',tanggalInput:'26/8/2020'},
        {id:'4',name:'Membuat Profile Screen',tanggalInput:'26/8/2020'},
    ])
    const [inputan,setInputan]=useState('')


    //Delete berfungsi, tapi terlambat satu inputan (tidak realtime, terhapus tapi tidak kelihatan). Ada saran?
    const deleteData=(id)=>{
        var test = data.splice(id-1,1)
        console.log(test)
        console.log(data)
        setData(data)
    }

    const tambahData=(text)=>{
        setData([...data,{id:Number(data[data.length-1].id)+1,name:text,tanggalInput:`${date}/${month}/${year}`}])

        // setData([...data,{id:data.length+=1,name:text,tanggalInput:`${date}/${month}/${year}`}])

        setInputan('')
        alert(`Anda Telah Menambahkan List: `+text)

        // alert(data.length)
    }


    //Deklarasi Tanggal
    var date = new Date().getDate()
    var month = new Date().getMonth() + 1; 
    var year = new Date().getFullYear();

    return (
        <View style={{height:'100%',width:'100%'}}>
            <Text style={{paddingTop:10,paddingLeft:8}}>
                Masukan To Do List:
            </Text>
            
            <View style={{flexDirection:'row',marginTop:4,paddingLeft:8}}>
                <TextInput 
                    style={{width:'80%',borderColor:'black',borderWidth:1}}
                    placeholder='input here'
                    onChangeText={text => setInputan(text)}
                    value={inputan}
                >
                </TextInput>
                
                <View style={{width:'15%',alignSelf:'center', marginLeft:10}}>
                    <Button                
                        title='+'
                        onPress={()=>tambahData(inputan)}
                    >
                    </Button>
                </View>
            </View>

            

            <FlatList
                keyExtractor={(item) => item.id.toString()}
                data={data}
                renderItem={({ item }) => (
                    <View style={{alignItems:'center'}}>

                        <View style={{width:'85%',borderColor:'black',borderWidth:1,height:60,flexDirection:'row',marginTop:30}}>
                            <View style={{flex:7,justifyContent: 'center',marginLeft:15}}>
                                <Text>
                                    {item.tanggalInput}
                                </Text>
                                <Text>
                                    {item.name}
                                </Text>
                            </View>

                        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <TouchableOpacity
                                onPress={()=>deleteData(item.id)}
                            >
                                <Icon
                                    name="trash"
                                    color='black'
                                    size={30}
                                >
                                </Icon>
                            </TouchableOpacity>
                        </View>
                        </View>

                    </View>
            )}
        >

        </FlatList>

            
        </View>
    )
}

export default ToDoList