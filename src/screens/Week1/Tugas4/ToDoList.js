import React, { useContext } from 'react'
import {View, Text, TextInput, Button, TouchableOpacity, FlatList} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome';

const ToDoList = () =>{
    
    const state = useContext(RootContext)
    console.log(state)

    const renderItem=({item,index})=>{
        return (
            <View style={{alignItems:'center'}}>

                <View style={{width:'85%',borderColor:'black',borderWidth:1,height:60,flexDirection:'row',marginTop:30}}>
                    <View style={{flex:7,justifyContent: 'center',marginLeft:15}}>
                        <Text>
                            {item.date}
                        </Text>
                        <Text>
                            {item.title}
                        </Text>
                    </View>
    
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <TouchableOpacity
                    onPress={()=>state.removeToDos(index)}
                >
                    <Icon
                        name="trash"
                        color='black'
                        size={30}
                    >
                    </Icon>
                </TouchableOpacity>
                </View>
                </View>
            
            </View>
        )
    }


    return (
        
        <View style={{height:'100%',width:'100%'}}>
            <Text style={{paddingTop:10,paddingLeft:8}}>
                Masukan To Do List:
            </Text>
            
            <View style={{flexDirection:'row',marginTop:4,paddingLeft:8}}>
                <TextInput 
                    style={{width:'80%',borderColor:'black',borderWidth:1}}
                    placeholder='input here'
                    value={state.input}
                    onChangeText={(value) => state.handleChangeInput(value)}
                    
                >
                </TextInput>
                
                <View style={{width:'15%',alignSelf:'center', marginLeft:10}}>
                    <Button                
                        title='+'
                        onPress={()=> state.addTodos()}
                    >
                    </Button>
                </View>
            </View>
            
            <FlatList
                data={state.todos}
                renderItem={renderItem}
            >
            </FlatList>


            
        </View>
    )
}

export default ToDoList







    {/* <FlatList
                data={state.todos}
                renderItem={renderItem}
             >
            </FlatList> */}



// const renderItem=({item})=>{
//     <View style={{alignItems:'center'}}>

//         <View style={{width:'85%',borderColor:'black',borderWidth:1,height:60,flexDirection:'row',marginTop:30}}>
//             <View style={{flex:7,justifyContent: 'center',marginLeft:15}}>
//                 <Text>
//                     {item.date}
//                 </Text>
//                 <Text>
//                     {item.title}
//                 </Text>
//             </View>

//         <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
//             <TouchableOpacity
//                 onPress={()=>deleteData(item.title)}
//             >
//                 <Icon
//                     name="trash"
//                     color='black'
//                     size={30}
//                 >
//                 </Icon>
//             </TouchableOpacity>
//         </View>

//         </View>

//     </View>
// }