import React, { useState, createContext } from 'react'
import ToDoList from './ToDoList'

export const RootContext = createContext()

const Context = () =>{

    const [input,setInput] = useState('')
    const [todos,setTodos] = useState([])

    const handleChangeInput=(value)=>{
        setInput(value)
        console.log(input)
    }

    const addTodos=()=>{
        var day = new Date().getDate()
        var month = new Date().getMonth() + 1; 
        var year = new Date().getFullYear();
        var today = `${day}/${month}/${year}`

        setTodos([...todos,{title:input,date:today}])
        setInput('')
    }

    const removeToDos=(id)=>{
        setTodos(
            todos.filter((index) => {
                if(index !== id) {
                    return true
                }
            })
        )
    }

    return (
        <RootContext.Provider value={{input,todos,handleChangeInput,addTodos,removeToDos}}>
            <ToDoList/>        
        </RootContext.Provider>

    )



}

export default Context