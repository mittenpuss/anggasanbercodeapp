import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const Intro=()=>{
    return (
        <View style={styles.container}>
            <Text style={styles.mainText}>
               Hallo Kelas React Native Lanjutan Sanbercode!
            </Text>
        </View>
    )
}


const styles = StyleSheet.create({
    mainText: {
        height:'100%',
        textAlign:'center',
        textAlignVertical:'center'
    }
})

export default Intro;