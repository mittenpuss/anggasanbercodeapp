import React, { useState } from 'react'
import { View, Text, Image, TextInput } from 'react-native'
import LogoJenius from '../Pictures/jeniusLogoPutih.png'
import { Icon } from 'react-native-elements'


const pin = () =>{

    const [input,setInput]=useState(0)

    const jumlahInput=(text)=>{
        setInput(text.length)
    }

    return (
        <View style={{
            height:'100%',
            width:'100%',
            
        }}>

            <View style={{alignItems:'center',marginTop:20}}>
                <Image 
                    source={LogoJenius}
                    style={{width:120,height:27}}
                >
                </Image>

                <Text style={{color:'white', marginTop:55,fontSize:25}}>
                    Masukkan PIN
                </Text>


            <View style={{display:'flex', flexDirection:'row',justifyContent:'space-evenly',width:'70%',marginTop:'10%'}}>
                {input>=1?
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='white'
                        size={16}
                    />
                :
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='#517fa4'
                        size={16}
                    />    
                 }
                
                {input>=2?
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='white'
                        size={16}
                    />
                :
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='#517fa4'
                        size={16}
                    />    
                 }

                {input>=3?
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='white'
                        size={16}
                    />
                :
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='#517fa4'
                        size={16}
                    />    
                 }

                {input>=4?
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='white'
                        size={16}
                    />
                :
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='#517fa4'
                        size={16}
                    />    
                 }

                {input>=5?
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='white'
                        size={16}
                    />
                :
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='#517fa4'
                        size={16}
                    />    
                 }

                {input>=6?
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='white'
                        size={16}
                    />
                :
                    <Icon
                        name='circle'
                        type='font-awesome'
                        color='#517fa4'
                        size={16}
                    />    
                 }
            </View>
                
            <View style={{alignItems:'center'}}>
                <Text style={{color:'white', marginTop:50,fontSize:18, opacity:0.8}}>
                    Lupa Pin?
                </Text>
                <Text style={{color:'white', marginTop:5,fontSize:15,fontWeight:'bold'}}>
                    GUNAKAN PASSWORD
                </Text>
            </View>

            <TextInput
            placeholder='masukkan angka'
            keyboardType='number-pad'
            onChangeText={(text)=>jumlahInput(text)}
            >

            </TextInput>

            </View>
            
            {input===6?
            alert('udah 6 cuy!!')
            :
            null
            }

        </View>
    )
}

export default pin