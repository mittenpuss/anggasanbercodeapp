import React, { useState } from 'react';
import { TextInput, View, Button, Text } from 'react-native';

const UselessTextInput = () => {

  const [value, onChangeText] = useState('Useless Placeholder');
  const [entry, textEntry] = useState(true);

  changeTextEntry=()=>{
    textEntry(!entry)
  }

  return (
    <View>
        <TextInput
            placeholder='Masukkan Password'
            style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
            onChangeText={text => onChangeText(text)}
            secureTextEntry={entry}
            // value={value}
        />
        <Button
            onPress={changeTextEntry}
            title="Ntaps"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
        />
    </View>

  );
}

export default UselessTextInput;