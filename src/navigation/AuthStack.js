import React, {useState,useEffect} from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'


import Login from '../screens/Week2/Login'
import SplashScreen from '../screens/Week2/Splashscreen'
import Intro from '../screens/Week2/Intro'
import Register from '../screens/Week2/Register'
import HomeTab from './HomeTab'
import AsyncStorage from '@react-native-community/async-storage'


const Stack = createStackNavigator()

const MainNavigation = () => (
    
    <Stack.Navigator headerMode='none'>
        <Stack.Screen name ="Intro" component={Intro}/>
        <Stack.Screen name ="Login" component={Login}/>
        <Stack.Screen name ="HomeTab" component={HomeTab}/>
        <Stack.Screen name ="Register" component={Register}/>
    </Stack.Navigator>
)

const AppNavigation = () => {

    const [ isLoading, setIsLoading ] = useState(true)

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)



    }, [])

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation/>
        </NavigationContainer>
        
    )
}

export default AppNavigation;