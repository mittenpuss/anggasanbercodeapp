import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import DashBoard from './../screens/Week2/Dashboard'
import Chart from './../screens/Week2/Chart'


const Stack = createStackNavigator()

const HomeStack = () =>{
    return (
    
    <Stack.Navigator>
        <Stack.Screen name='Home' component={DashBoard} options={{headerShown: false}}/>
        <Stack.Screen name ="React Native" component={Chart}/>
    </Stack.Navigator>
    )
    
}

export default HomeStack