import React, {useState,useEffect} from 'react'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Profile from '../screens/Week2/Profile'
import Maps from '../screens/Week2/Maps'
import Chat from '../screens/Week2/Chat'

import Ionicons from 'react-native-vector-icons/Ionicons'
import HomeStack from './HomeStack'



const Tab=createBottomTabNavigator()

const HomeTab = () => {
    return (
    <Tab.Navigator
            screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;
    
                if (route.name === 'Home') {
                iconName = focused
                    ? 'home'
                    : 'home-outline';
                } else if (route.name === 'Maps') {
                iconName = focused 
                    ? 'map' 
                    : 'navigate';
                } else if (route.name === 'Chat') {
                    iconName = focused 
                        ? 'chatbubble-ellipses-sharp' 
                        : 'chatbubble-ellipses-outline';
                } else if (route.name === 'Profile') {
                    iconName = focused 
                        ? 'journal-sharp' 
                        : 'journal-outline';
                } 
    
                // You can return any component that you like here!
                return <Ionicons name={iconName} size={size} color={color} />;
            },
            })}
            
            tabBarOptions={{
            activeTintColor: '#41C4FF',
            inactiveTintColor: 'gray',
            }}>
            
            <Tab.Screen name='Home' component={HomeStack}/>
            <Tab.Screen name='Maps' component={Maps}/>
            <Tab.Screen name='Chat' component={Chat}/>
            
            <Tab.Screen name="Profile" component={Profile}/>
    
    </Tab.Navigator>
    )
}

export default HomeTab